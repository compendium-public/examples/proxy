# Docker Compose Example

This will start a proxy attach to the default docker network and redirect traffic for 3 endpoint paths. 

''Note: In this example we define two additional services which do not exist (service1 and service2)...  You should create those.''

## Run

Foreground

<pre>
docker-compose up
</pre>

Background

<pre>
docker-compose up -d
docker-compose logs -f
</pre>

Browse to: http://localhost:7777/api/v1/httpenv/
